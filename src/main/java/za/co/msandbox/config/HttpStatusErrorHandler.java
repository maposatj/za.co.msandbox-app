/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.msandbox.config;

import com.google.gson.JsonObject;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author takalani.maposa
 */
@ControllerAdvice
public class HttpStatusErrorHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {Exception.class, RuntimeException.class})
    @ResponseBody
    public String handleConflict(Exception e) {
        e.printStackTrace();
        JsonObject jo = new JsonObject();
        jo.addProperty("responseMessage", e.getLocalizedMessage());
        return jo.toString();
    }
}
