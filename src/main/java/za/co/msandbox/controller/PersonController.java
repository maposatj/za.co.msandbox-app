/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.msandbox.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import za.co.msandbox.model.BaseModel;
import za.co.msandbox.model.Person;
import za.co.msandbox.service.PersonService;

/**
 *
 * @author vhusha
 */
@RestController
@RequestMapping("/person")
public class PersonController {

    @Autowired
    PersonService personService;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<Person> persons() {
        return personService.findAll();
    }

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    @ResponseBody
    public String getPerson() {

        JsonObject jo = new JsonObject();
        jo.addProperty("username", "takalani");
        jo.addProperty("name", "Takalani");
        jo.addProperty("surname", "Maposa");
        jo.add("personDetails", new Gson().toJsonTree(personService.getOne()));

        return jo.toString();
    }

    @RequestMapping(value = "/hello", method = RequestMethod.POST)
    @ResponseBody
    public String savePerson(@RequestBody BaseModel<Person> model) {
        Person person = personService.save(model.getData());
        return new Gson().toJson(person);
    }

    @RequestMapping(value = "/bln")
    @ResponseBody
    public List<Person> byLastName(@RequestParam("lastName") String lastName) {

        Person person = new Person();
        person.setFirstName("First");
        person.setLastName("Test");
        person = personService.save(person);

        return personService.google(lastName);
    }
}
