/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.msandbox.controller;

import com.google.gson.JsonObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author takalani.maposa
 */
@RestController
@RequestMapping("/zample")
public class TheeController {

    @ResponseBody
    @RequestMapping(method = RequestMethod.GET)
    public String nowThatsCool() {
        JsonObject jo = new JsonObject();
        return jo.toString();
    }
}
