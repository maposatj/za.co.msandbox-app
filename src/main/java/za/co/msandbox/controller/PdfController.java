/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.msandbox.controller;

import br.eti.mertz.wkhtmltopdf.wrapper.Pdf;
import br.eti.mertz.wkhtmltopdf.wrapper.page.PageType;
import br.eti.mertz.wkhtmltopdf.wrapper.params.Param;
import com.google.gson.Gson;
import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import za.co.msandbox.model.BaseModel;
import za.co.msandbox.service.ConfigurationService;
import za.co.msandbox.service.UserService;

/**
 *
 * @author takalani.maposa
 */
@RestController
@RequestMapping("/pdf")
public class PdfController {

    @Autowired
    ConfigurationService configurationService;

    @Autowired
    UserService userService;

    private static final Logger logger = LoggerFactory.getLogger(PdfController.class);

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public String generatePdf(@RequestBody BaseModel<Map<String, String>> model) {
//        JsonObject jo = new Gson().fromJson(null, null)
        logger.info("generatePdf data: {}", model.getData());

        Map<String, String> input = model.getData();

        String rawInput = new String(org.apache.commons.codec.binary.Base64.decodeBase64(input.get("input")));

//        Jsoup js;
//        org.jsoup.nodes.Document document = Jsoup.parse(rawInput);
//        rawInput = document.html();
        Pdf pdf = new Pdf();

        pdf.addPage(rawInput, PageType.htmlAsString);

// Add a Table of contents
        pdf.addToc();

// The `wkhtmltopdf` shell command accepts different types of options such as global, page, headers and footers, and toc. Please see `wkhtmltopdf -H` for a full explanation.
// All options are passed as array, for example:
        pdf.addParam(new Param("--no-footer-line"), new Param("--html-header", "file:///header.html"));
        pdf.addParam(new Param("--enable-javascript"));

//        ITextRenderer itr = new ITextRenderer();
//        itr.setDocumentFromString(rawInput);
//        itr.layout();
        Map<String, String> data = new HashMap<>();
//        try {
//            //os.toByteArray()
//            itr.createPDF(os);
        try {
            String pdfData = org.apache.commons.codec.binary.Base64.encodeBase64String(pdf.getPDF());
            data.put("pdfData", pdfData);
            logger.info("pdfData: ", pdfData);
        } catch (Exception e) {
            logger.error("error pdf.getPDF {}: ", e.getMessage());
        }
//        } catch (Exception e) {
//            logger.error("error itr.createPDF {}: ", e.getMessage());
//        } finally {
//            try {
//                os.close();
//            } catch (Exception ex) {
//                logger.warn("close os {}: ", ex.getMessage());
//            }
//        }
        Map<String, String> response = new HashMap<>();
        response.put("data", new Gson().toJson(data));
        response.put("responseCode", "0");
        response.put("responseMessage", "success");

        logger.info("generatePdf response: {}", response);

        return new Gson().toJson(response);
    }
}
