/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.msandbox.controller;

import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import za.co.msandbox.service.MailService;
import za.co.msandbox.service.SmsService;

/**
 *
 * @author takalani.maposa
 */
@RequestMapping("/deploy")
@RestController
public class Deploy {

    @Autowired
    SmsService smsService;

    @Autowired
    MailService mailService;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public String testDeploy(@RequestBody String body) throws Exception {
//        SmsModel smsModel = new SmsModel();
//        smsModel.setText("Deploy success");
//        smsModel.setTo(new String[]{
//            "27713640002"
//        });

//        mailService.sendMail("maposatakalani@gmail.com", deploStatus);
        JsonObject rp = new JsonObject();
        rp.addProperty("DEPLOY_QUEUED", "OK");
        return rp.toString();
    }
}
