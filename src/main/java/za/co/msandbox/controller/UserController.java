/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.msandbox.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import za.co.msandbox.model.BaseModel;
import za.co.msandbox.model.User;
import za.co.msandbox.service.ConfigurationService;
import za.co.msandbox.service.UserService;
import za.co.msandbox.util.AnnotationExclusionStrategy;

/**
 *
 * @author takalani.maposa
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    ConfigurationService configurationService;

    @Autowired
    UserService userService;

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public String addUser(@RequestBody BaseModel<Map<String, String>> model) {
//        JsonObject jo = new Gson().fromJson(null, null)
        logger.info("addUser data: {}", model.getData());

        User user = new User();
        user.setUserType(model.getData().get("userType"));
        user.setEmail(model.getData().get("email"));

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        user.setPassword(passwordEncoder.encode(model.getData().get("password")));

        Gson gson = new GsonBuilder().setExclusionStrategies(new AnnotationExclusionStrategy()).create();

        return gson.toJson(userService.save(user));
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public String login() {
        return null;
    }
}
