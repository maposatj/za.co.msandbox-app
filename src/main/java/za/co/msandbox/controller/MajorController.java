/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.msandbox.controller;

import com.google.gson.JsonObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author takalani.maposa
 */
@RestController
public class MajorController {

    @RequestMapping("/test")
    @ResponseBody
    public String test() {

        JsonObject jo = new JsonObject();
        jo.addProperty("username", "takalani");
        jo.addProperty("name", "Takalani");
        jo.addProperty("surname", "Maposa");
        return jo.toString();
    }
}
