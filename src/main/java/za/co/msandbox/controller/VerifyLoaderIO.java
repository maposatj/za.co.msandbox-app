/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.msandbox.controller;

import com.google.gson.JsonObject;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author takalani.maposa
 */
@RequestMapping("/loaderio-a61959cf8177a5e4eb63d37f1f3f5f78")
@RestController
public class VerifyLoaderIO {

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    @ResponseBody
    public String verify() {
        JsonObject jo = new JsonObject();
        jo.addProperty("username", "takalani");
        jo.addProperty("name", "Takalani");
        jo.addProperty("surname", "Maposa");
        return "loaderio-a61959cf8177a5e4eb63d37f1f3f5f78";
    }
}
