/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.msandbox.util;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.msandbox.model.SmsModel;

/**
 *
 * @author vhusha
 */
public class ClickatellSmsProvider implements SmsProvider {

    private final String token;
    private final String host;

    private static final Logger logger = LoggerFactory.getLogger(ClickatellSmsProvider.class);

    public ClickatellSmsProvider(Properties properties) {
        token = properties.getProperty("token");
        host = properties.getProperty("host");
    }

    public SmsResponse sendMessage(SmsModel smsModel) {

        Map<String, String> headers = new HashMap<>();
        headers.put("X-Version", "1");
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", "bearer " + token);
        headers.put("Accept", "application/json");

        SmsResponse response = new SmsResponse();
        SmsResponses responseCode;
        String responseMessage;
        logger.info("to send: {}", smsModel);
        try {
            HttpResponse<JsonNode> jsonResponse = Unirest.post(host + "/message")
                    .headers(headers)
                    .body(new Gson().toJson(smsModel))
                    .asJson();
            String responseBody = jsonResponse.getBody().toString();

            logger.info("Clickatell sendSms response: " + responseBody);

            if (jsonResponse.getStatus() == 202) {
                responseCode = SmsResponses.SMS_OK;
                responseMessage = "Message sent";
            } else {
                responseCode = SmsResponses.SMS_ERROR;
                JsonObject je = new Gson().fromJson(responseBody, JsonObject.class);
                logger.info("Clickatell toJson response: " + je.toString());
                responseMessage = je.get("data").getAsJsonObject().get("message").getAsJsonObject().get("error").getAsJsonObject().get("description").getAsString();
            }
            System.err.println("Here");
        } catch (Exception e) {
            logger.error("sendSms error: {}", e);
            responseCode = SmsResponses.SMS_ERROR;
            responseMessage = e.getMessage();
        }
        response.setResponseCode(responseCode);
        response.setResponseMessage(responseMessage);
        return response;
    }

    @Override
    public SmsResponse sendMessage(String text, String[] to) {
        SmsModel smsModel = new SmsModel();
        smsModel.setText(text);
        smsModel.setTo(to);
        return sendMessage(smsModel);
    }

}
