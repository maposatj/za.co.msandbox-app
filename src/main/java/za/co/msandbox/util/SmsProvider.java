/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.msandbox.util;

/**
 *
 * @author vhusha
 */
public interface SmsProvider {

    public SmsResponse sendMessage(String message, String[] to);
}
