/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.msandbox.util;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author vhusha
 */
public class RestError {

    @SerializedName("code")
    public int code;
    
    @SerializedName("error")
    public String errorDetails;
}
