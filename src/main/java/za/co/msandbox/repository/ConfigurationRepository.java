/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.msandbox.repository;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import za.co.msandbox.model.Configuration;

/**
 *
 * @author vhusha
 */
@Component
@Qualifier("configurationRepository")
public interface ConfigurationRepository extends JpaRepository<Configuration, Long> {

    public Configuration findByKey(String key);
}
