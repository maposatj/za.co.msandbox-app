/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.msandbox.service;

import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

/**
 *
 * @author takalani.maposa
 */
@Service
public class MailService {

    @Autowired
    private MailSender mailSender;

    public String sendMail(String to, String message) {
        JsonObject jo = new JsonObject();
        
        SimpleMailMessage smm = new SimpleMailMessage();
        smm.setFrom("maposatakalani@gmail.com");
        smm.setTo(to);
        smm.setSubject("Testing");
        smm.setText(message);
        mailSender.send(smm);
        
        return jo.toString();
    }
}
