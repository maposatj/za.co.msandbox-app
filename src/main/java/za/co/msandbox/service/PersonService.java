/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.msandbox.service;

import java.util.List;
import za.co.msandbox.repository.PersonRepository;
import za.co.msandbox.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 *
 * @author vhusha
 */
@Service
@Repository
public class PersonService {

    @Autowired
    PersonRepository personRepository;

    public Person getOne() {
        return personRepository.findOne(new Long(1));
    }

    public Person save(Person save) {
        return personRepository.saveAndFlush(save);
    }

    public List<Person> findAll() {
        return personRepository.findAll();
    }

    public List<Person> google(String lastName) {
        return personRepository.google(lastName);
    }

}
